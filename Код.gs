// Функция фильтрует значения таблицы по дате
function timer() {

  let table = SpreadsheetApp.getActiveSpreadsheet();
  let sheet = table.getSheetByName("Лист1");
  
  // Берем данные из таблицы
  // 2 строка, 2 столбец, последня строка-1, 6 столбец
  let data = sheet.getRange(2, 1, sheet.getLastRow(), 2).getValues();
  
  var checkLable = 0; // Ин-я переменной для "умного" выведения заголовка при наличии данных для отправки
  
  // Разбираем данные на отдельные колонки
  for (i = 0; i < (data.length - 1); i++) {
    // Отделяем первую строку
    let dataInfo = data[i]; Logger.log(dataInfo);
    
    let clientName = dataInfo[0]; // Logger.log(clientName); // Отделяем имя клиента
    
    // Фильтруем значния из таблицы относительно текущей даты
    let MILLIS_PER_DAY = 1000 * 60 * 60 * 24;
    let now = new Date(); // Logger.log(now);
    
    // Сравниваем даты текущую и табличное значение
    if (dataInfo[1].getTime() >= (now.getTime() - MILLIS_PER_DAY) && dataInfo[1].getTime() <= (now.getTime() + 7 * MILLIS_PER_DAY)) // Logger.log("true")
    {
      if (checkLable == 0) {
        sendText("🏛" + " " + "На ближайшую неделю назначены следующие дела:");
        checkLable = 1; 
      }
      sendText(clientName + " - " + "заседание суда назначено на " + Utilities.formatDate(dataInfo[1], "Europe/Moscow", "yyyy.MM.dd"));
    }
  }
}


// Функция для отправки сообщений в телеграмм
function sendText(text, keyBoard) {
  
  let data = {
    method: "post",
    payload: {
      method: "sendMessage",
      chat_id: String(PropertiesService.getScriptProperties().getProperty("chat_id")),
      text: text,
      parse_mode: "HTML",
      reply_markup: JSON.stringify(keyBoard)
    }
  }
  
  let TELEGRAM_BOT_API_KEY = PropertiesService.getScriptProperties().getProperty("TELEGRAM_BOT_API_KEY");
  UrlFetchApp.fetch("https://api.telegram.org/bot" + TELEGRAM_BOT_API_KEY + "/", data);
}


// Функция onOpen() запускается при открытии документа и позволяе настраивать интерфес
// https://developers.google.com/apps-script/guides/triggers/#onopen
function onOpen() {

  if (PropertiesService.getScriptProperties().getProperty("TELEGRAM_BOT_API_KEY") == null) {
    showSidebar()
  }
  if (PropertiesService.getScriptProperties().getProperty("chat_id") == null) {
    showSidebar()
  }
    
  // Создаём новый пункт меню
  SpreadsheetApp.getUi()
    .createMenu("Telegram")
    .addItem("Настройка связи", "showSidebar")
    .addItem("Тест связи", "timer")
    .addToUi();
  // https://developers.google.com/apps-script/reference/base/ui#createmenucaption
  // https://developers.google.com/apps-script/reference/base/ui#createaddonmenu
  
}


// Функция открывае сайдбар. Интерфейс описан в sidebar.html
function showSidebar() {
  SpreadsheetApp.getUi().showSidebar(HtmlService
                                     .createTemplateFromFile("sidebar.html")
                                     .evaluate()
                                     .setTitle("Настройка связи с Telegram"));
  // https://developers.google.com/apps-script/reference/base/ui#showsidebaruserinterface
}


// Функция записывает настройки указанные в полях ввода в Свойства проекта
function writeSettings(e) {
  
  ScriptProperties.setProperty("TELEGRAM_BOT_API_KEY", e.TELEGRAM_BOT_API_KEY);
  ScriptProperties.setProperty("chat_id", e.chat_id);
  
  return "Запись произведена успешно!";  
}


// Функция считывает настойки указанные в Свойствах проекта и позволяет их записать в поля ввода при открытии сайдбара
function readSettings() {
  return [
    PropertiesService.getScriptProperties().getProperty("TELEGRAM_BOT_API_KEY"),
    PropertiesService.getScriptProperties().getProperty("chat_id")
  ];
}